    import XCTest
    @testable import AutoTrackingCamera

    final class AutoTrackingCameraTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(AutoTrackingCamera().text, "Hello, World!")
        }
    }
